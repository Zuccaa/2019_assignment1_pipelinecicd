# 2019_assignment1_PipelineCICD
First assignment made by:
*  *Stefano Zuccarella (816482)*
*  *Matteo Paolella (816933)*

The work has been done by both the members in the same measure for all the period of time (50%-50%).
# Application
The app used for the pipeline is a simple webapp created using Java Servlet and JSP. It is also connected to a PostgreSql database containing the data of all the drivers of the 2017 F1 Championship.

As input the number of an F1Pilot is inserted. As output, if that number is associated to an existing driver, the data about him is returned (plus the next number is returned).
# GitLab Repository
The official repository for this assignment is [https://gitlab.com/Zuccaa/2019_assignment1_pipelinecicd].
# Pipeline stages
For this assignment, all of the expected stages of the CI/CD pipeline have been implemented:
1. **Build**
2. **Verify**
3. **Unit-test**
4. **Integration-test**
5. **Package**
6. **Release**
7. **Deploy**

In the following list, a briefly description for each stage is reported:
1. In the build phase, the build is done using mvn compile.
2. In the verify phase, a static analysis to the code is done using the checkstyle and spotbugs plugins. In this phase, the failure has been allowed because the fix of the warnings returned by these plugin is out of the scope of this assignment.
3. In the unit-test phase, an easy test is run: given a number in input, the successive number must be returned.
4. In the integration-test phase, the connection between the database and the Java classes is tested: given a number in input, the right data must be returned.
5. In the package phase, the executable war of the application is created using the command mvn package and the plugin webapp-runner.
6. In the release phase, the container that contains the war is created using docker commands.
7. In the deploy phase, the container is pushed to the container of Heroku and the application is deployed there using the webapp-runner.

# Notes
To use the application locally, the database needs to be created using PgAdmin4. The necessary script in order to create the proper database is CreateDriverTable.sql. Then, to allow the connection, in the class getPilotController the url, the user and the password needs to be modified properly.