package unimib.assignment1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import unimib.assignment1.dao.PilotDao;
import unimib.assignment1.model.Pilot;
import unimib.assignment1.DummyClass;

/**
 * Servlet implementation class GetPilotController
 */
public class GetPilotController extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int numberPilot = Integer.parseInt(request.getParameter("numberPilot"));
		PilotDao dao = new PilotDao();
		Pilot p = dao.getPilot(numberPilot, "jdbc:postgresql://localhost/F1Driver", "postgres", "Zuccaa97");
		
		request.setAttribute("pilot", p);
		int nextNumberPilot = new DummyClass().nextNumber(numberPilot);
		request.setAttribute("nextNumber", nextNumberPilot);
		RequestDispatcher rd = request.getRequestDispatcher("showPilot.jsp");
		rd.forward(request, response);
	}

}
