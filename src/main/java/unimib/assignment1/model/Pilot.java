package unimib.assignment1.model;

public class Pilot {

	private int numberPilot;
	private String name;
	private String surname;
	private String nationality;
	
	public int getNumberPilot() {
		return numberPilot;
	}
	public void setNumberPilot(int numberPilot) {
		this.numberPilot = numberPilot;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	@Override
	public String toString() {
		return "Pilot [numberPilot=" + numberPilot + ", name=" + name + ", surname=" + surname + ", nationality="
				+ nationality + "]";
	}
	
}
