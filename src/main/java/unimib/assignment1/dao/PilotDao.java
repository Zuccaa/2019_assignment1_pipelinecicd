package unimib.assignment1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import unimib.assignment1.model.Pilot;

public class PilotDao {

	public Pilot getPilot (int numberPilot, String path, String user, String psw) {
		
		Pilot p = new Pilot();
		Connection con;
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(path, user, psw);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from driver where CodPilota = " + numberPilot);
		
			if (rs.next()) {
				p.setNumberPilot(rs.getInt("CodPilota"));
				p.setName(rs.getString("Nome"));
				p.setSurname(rs.getString("Cognome"));
				p.setNationality(rs.getString("Nazionalita"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return p;
	}
}
