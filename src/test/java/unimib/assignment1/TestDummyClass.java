package unimib.assignment1;

import static org.junit.Assert.*;

import org.junit.Test;

import unimib.assignment1.DummyClass;

public class TestDummyClass {

	@Test
	public void testNextNumber() {
		int nextNumber = new DummyClass().nextNumber(5);
		
		assertEquals(nextNumber, 6);
	}

}