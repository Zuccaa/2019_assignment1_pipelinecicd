package unimib.assignment1;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import unimib.assignment1.dao.PilotDao;
import unimib.assignment1.model.Pilot;

public class DummyIntegrationTest {

	/*@Test
	public void prova() throws ClassNotFoundException, SQLException {
		Class.forName("org.postgresql.Driver");
        Connection con = DriverManager.getConnection(
                "jdbc:postgresql://postgres:5432/F1Driver", "postgres", "root");
        Statement st = con.createStatement();
        // ResultSet rs = st.executeQuery("select * from driver where CodPilota = 5");
        ResultSet rs = st.executeQuery("SELECT 'OK' AS status");
        
        assertEquals(5,5);
	}*/
	
	@Test
	public void getDriver() {
		
		PilotDao pd = new PilotDao();		
		Pilot p = new Pilot();
		
		p = pd.getPilot(5, "jdbc:postgresql://postgres:5432/F1Driver", "postgres", "root");
		
		assertEquals(p.toString(), "Pilot [numberPilot=5, name=Sebastian, surname=Vettel, nationality=Germania]");
	}
	
}
