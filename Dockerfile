FROM openjdk:8-jdk-slim
ENV TZ=Europe/Rome

RUN apt-get update && apt-get install -y curl

WORKDIR /srv/app/
COPY ./target/DummyF1.war .app.war
COPY ./target/dependency/webapp-runner.jar webapp-runner.jar

#"--port", "$PORT", 
ENTRYPOINT ["java", "-jar", "webapp-runner.jar", "app.war"]
HEALTHCHECK --interval=1m --timeout=3s CMD curl -f http://localhost:8080/_meta/health || exit 1